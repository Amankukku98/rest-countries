import axios from 'axios';
export const getCountries=async()=>
{
 try{
    const response= await axios.get('https://restcountries.com/v3.1/all');
    return response.data;
 }catch(error){
    console.log(error);
    throw error;
 }
}
export const getEachCountries=async(name)=>
{
 try{
    const response= await axios.get(`https://restcountries.com/v3.1/name/${name}`);
    return response.data;
 }catch(error){
    console.log(error);
    throw error;
 }
}
export const getCountriesByBorder=async(border)=>
{
 try{
    const response= await axios.get(`https://restcountries.com/v3.1/alpha/${border}`);
    return response.data;
 }catch(error){
    console.log(error);
    throw error;
 }
}