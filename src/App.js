import React from 'react'
import CountryList from './components/CountryList';
import './App.css';
import Header from './components/Header';
import CountryDetails from './components/CountryDetails';
import {BrowserRouter,Routes,Route} from 'react-router-dom';
function App() {
  return (
    <div>
            <BrowserRouter>
            <Header/>
            <Routes>
            <Route exact path="/" element={<CountryList/>}></Route>
            <Route exact path="/countryDetails/:name" element={<CountryDetails/>}></Route>
            </Routes>
            </BrowserRouter>
      
    </div>
  )
}

export default App