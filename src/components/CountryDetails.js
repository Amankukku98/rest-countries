import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import * as countryApi from '../backend/api';
import Button from 'react-bootstrap/Button';
import { NavLink,useNavigate } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { BsArrowLeft } from 'react-icons/bs';
import '../styles/Header.css';
import Spinner from './Loadder';
function CountryDetails() {
    let navigate=useNavigate();
    const params = useParams();
    const { name } = params;
    const [borders, setBorders] = useState([]);
    const [countryDetails, setCountryDetails] = useState([]);
    const [isLoading,setIsLoading]=useState(false);
    useEffect(() => {
        getEachCountryDetails(name);
    }, [])
    const getEachCountryDetails = (name) => {
        setIsLoading(true);
        countryApi.getEachCountries(name).then((response) => {
            if (response[0].borders !== undefined) {
                setBorders(response[0].borders);
            }
            setCountryDetails(response)
            setIsLoading(false);
        }).catch((error) => {
            setIsLoading(false);
            console.log(error);
        })
    }
    useEffect(() => {
        // Add event listener for the "keydown" event on the document
        const handleKeyDown = (event) => {
          if (event.key === 'Escape') {
            // Navigate back when the "Escape" key is pressed
            handleBack();
          }
        };
        document.addEventListener('keydown', handleKeyDown);
        // Clean up the event listener when the component unmounts
        return () => {
          document.removeEventListener('keydown', handleKeyDown);
        };
      }, []);
    const handleBack=()=>{
    navigate("/");
    }
    const changeBorderDetails=(border)=>{
        setIsLoading(true);
        setBorders([]);
        setCountryDetails([]);
        countryApi.getCountriesByBorder(border).then((response)=>{
            if (response[0].borders !== undefined) {
                setBorders(response[0].borders);
            }
            setCountryDetails(response);
            setIsLoading(false);
        }).catch((error)=>{
            console.log(error);
            setIsLoading(false);
        })
    }
    // Function to get the official native name
    const getOfficialNativeName=(country)=> {
        const nativeName = country.name.nativeName;
        // find returns always first occurrence
        const officialNativeName = Object.values(nativeName).find(
          (name) => name.official
        );
      
        return officialNativeName ? officialNativeName.official : "Not found";
      }
       // Function to get the currency name
  const getCurrencyName=(country)=>{
    const courrency = country.currencies;
    let currencyName=Object.values(courrency)
   return currencyName[0].name;
  }
    // Function to get the language
    const getLanguage=(country)=>{
        const language = country.languages;
        let allLanguage=Object.values(language)
       return allLanguage.join();
      }
    return (
        <>
            <NavLink to="/" className="nav-link">
                <Button onClick={handleBack} style={{marginTop:'65px',marginLeft:'15px',display:'flex',alignItems:'center'}} variant="danger">
                <BsArrowLeft className="mt-1" style={{ marginRight: '5px' }} />Back</Button>
            </NavLink>
            <Container className='mt-5'>
            {isLoading && <div className='d-flex justify-content-center vh-100'>
              <Spinner text="Loading..."/>
            </div>}
                {countryDetails.length>0 && <Row>
                <Col md={4}>
                    <div className="imge">
                    <img src={countryDetails[0].flags.png} alt="country" />
                    </div>
                </Col>
                <Col md={3} className='countryDetails'>
                    <h3 className='countryName'>{countryDetails[0].name.common}</h3>
                    <p><span className='first'>Native Name:</span><span>{getOfficialNativeName(countryDetails[0])}</span></p>
                    <p><span className='first'>Population:</span><span>{countryDetails[0].population}</span></p>
                    <p><span className='first'>Region:</span><span>{countryDetails[0].region}</span></p>
                    <p>                    <span className='first'>Sub Region:</span><span>{countryDetails[0].subregion}</span></p>
                    <span className='first'>Capital:</span><span className='firs'>{countryDetails[0].capital[0]}</span><br />
                </Col>  
                <Col md={3} className='countryDetails second'>
                <p><span className='first'>Top Level Domain</span><span>{countryDetails[0].tld}</span></p>
                    <p><span className='first'>Currency:</span><span>{getCurrencyName(countryDetails[0])}</span></p>
                    <span className='first'>Languages:</span><span>{getLanguage(countryDetails[0])}</span>
                </Col>
                </Row>}
                {countryDetails.length>0 && <Row>
                <Col md={4}></Col>
                <Col md={7} className='borders bordersBtn'>
                    <h5>Border Countries:</h5>
                    {countryDetails[0].borders===undefined ? <h5>No Borders</h5> : borders.map((border)=>{
                        return<Button key={border} onClick={()=>changeBorderDetails(border)} variant="success">{border}</Button>
                    })}
                </Col>
                </Row>}
            </Container>
        </>
    )
}

export default CountryDetails