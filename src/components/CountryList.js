import React, { useState, useEffect } from 'react';
import * as countryAPI from '../backend/api';
import Card from 'react-bootstrap/Card';
import ListGroup from 'react-bootstrap/ListGroup';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';
import '../App.css';
import { BsSearch } from 'react-icons/bs';
import { InputGroup, FormControl } from 'react-bootstrap';
import Spinner from './Loadder';
import {NavLink } from 'react-router-dom';
function CountryList() {
  const [countries, setCountries] = useState([]);
  const [region] = useState(['Africa', 'Asia', 'Americas', 'Europe', 'Oceania', 'Antarctic']);
  const [search, setSearch] = useState("");
  const [filterRegion, setFilterRegion] = useState("");
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    getCountries();
  }, []);

  const getCountries = () => {
    setIsLoading(true);
    countryAPI.getCountries().then((response) => {
      setCountries(response);
      setIsLoading(false);
    }).catch((error) => {
      console.log(error);
      setIsLoading(false);
    });
  }
  const filteredCountries = countries.filter((country) => {
    const lowerCaseCountryName = country.name.common.toLowerCase();
    const lowerCaseSearch = search.toLowerCase();
    // Check if the country name contains the search input and optionally check the region
    return lowerCaseCountryName.includes(lowerCaseSearch) &&
      (filterRegion === '' || country.region === filterRegion);
  });

  return (
    <>
      <Container className='mt-5'>
        <Row className='justify-content-end'>
        <Col md={3}>
      <InputGroup className='mt-4'>
      <InputGroup.Text className="inputBox">
            <BsSearch />
          </InputGroup.Text>
        <FormControl
        className="inputBox"
          type='text'
          placeholder='Search Here ...'
          onChange={(event) => setSearch(event.target.value)}
        />
      </InputGroup>
    </Col>
          <Col md={3}>
            <Form.Select aria-label="Default select example" className='mt-4 inputBox' onChange={(event) => setFilterRegion(event.target.value)}>
              <option>Filter Region</option>
              {region.map((region) => {
                return <option key={region} value={region}>{region}</option>
              })}
            </Form.Select>
          </Col>
        </Row>
      </Container>
      <Container className="mt-4">
        <Row>
          {isLoading && (
            <div className='d-flex justify-content-center vh-100'>
              <Spinner text="Loading..."/>
            </div>
          )}
          {filteredCountries.length<=0 && <div className='d-flex justify-content-center text-danger'>
            <h5>Data Not Found</h5>
          </div>}
          {filteredCountries.map((country,index) => (
            <Col key={index} md={4} lg={3} className='card-style'>
              <NavLink to={`/countryDetails/${country.name.common}`} className="nav-link">
              <Card style={{ width: '100%' }}>
                <Card.Img variant="top" src={country.flags.png} height="150" />
                <Card.Body>
                  <Card.Title>{country.name.common}</Card.Title>
                </Card.Body>
                <ListGroup className="list-group-flush">
                  <ListGroup.Item><span className='title'>Population:</span>{country.population}</ListGroup.Item>
                  <ListGroup.Item><span className='title'>Region:</span>{country.region}</ListGroup.Item>
                  <ListGroup.Item><span className='title'>Capital:</span>{country.capital}</ListGroup.Item>
                </ListGroup>
              </Card>
              </NavLink>
            </Col>
          ))}
        </Row>
      </Container>
    </>
  );
}

export default CountryList;
